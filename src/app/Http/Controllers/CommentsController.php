<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentsController extends Controller
{
    public function getAll(Request $request)
    {
        $comments = Comment::where('id', '>', 0);

        $article = $request->input('article');
        $user = $request->input('user');

        if ($article) {
            $comments = $comments->where('article_id', $article);
        }

        if ($user) {
            $comments = $comments->where('user_id', $user);
        }

        return $comments->orderBy('created_at', 'DESC')->get();
    }

    public function getOne($id)
    {
        return Comment::findOrFail($id);
    }

    public function create(Request $request)
    {
        return Comment::create([
            'article_id'    => $request->input('article_id'),
            'user_id'       => $request->input('user_id'),
            'content'       => $request->input('content'),
        ]);
    }

    public function update(Request $request, $id)
    {
        $comment = Comment::findOrFail($id);

        $comment->content = $request->input('content');

        $comment->save();

        return $comment;
    }

    public function deleteOne($id)
    {
        return Comment::destroy($id);
    }
}
